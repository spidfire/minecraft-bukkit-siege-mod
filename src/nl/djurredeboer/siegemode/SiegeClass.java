package nl.djurredeboer.siegemode;

import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Stack;


import org.bukkit.Location;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.inventory.ItemStack;

import net.minecraft.server.Item;
import nl.djurredeboer.siegemode.ColoredArmour.Armor;
import nl.djurredeboer.siegemode.ColoredArmour.ArmorColor;
import nl.djurredeboer.siegemode.User.*;
public class SiegeClass {
	boolean active_siege = false;
	int siegespot[];
	static Siegemode root;
	Player init_user;
	int timeout = 15;
	Stack<Backupblock> backup = new Stack<Backupblock>();
	Stack<Entity> mobs = new Stack<Entity>();
	Stack<Location> spawnlocations = new Stack<Location>();
	int foodlevel =15;
	
	Dictionary<String, BackupUser> backup_user=new Hashtable<String, BackupUser>();
	
	public void startsiege(Player player, Siegemode siegemode, String type, String team) {
		// player.get
		root = siegemode;
		active_siege = true;
		// setup the enviroment

		init_user = player;
		if(backup_user.get(player.getName()) == null){
			BackupUser b =  new BackupUser();
			b.store(player);
			backup_user.put(player.getName(),b);
		}
		if(team.toLowerCase().equals("yellow")){
			coloredArmour(player,ArmorColor.YELLOW);
		}else if(team.toLowerCase().equals("blue")){
			coloredArmour(player,ArmorColor.BLUE);
		}else if(team.toLowerCase().equals("green")){
			coloredArmour(player,ArmorColor.GREEN);
		}else{
			coloredArmour(player,ArmorColor.RED);
		}
		init_user.getServer().broadcastMessage("Player"+player.getDisplayName()+" has joined team: "+ team);
		System.out.println(type.toLowerCase() + " -> "+ team.toLowerCase());
		if(type.toLowerCase().equals("sniper") )
			sniper(player);
		else if(type.toLowerCase().equals("magic") )
			magic(player);
		else if(type.toLowerCase().equals("pyro") )
			pyro(player);
		else if(type.toLowerCase().equals("spleef") )
			spleef(player);
		else
			soldier(player);
		

	}
	
	public void coloredArmour(Player p, ArmorColor color){
		ItemStack helm = new ItemStack(Item.LEATHER_LEGGINGS.id);
		helm = Armor.setColor(helm, color);
		p.getInventory().setHelmet(helm);
		
		ItemStack boots = new ItemStack(Item.LEATHER_BOOTS.id);
		boots = Armor.setColor(boots, color);
		p.getInventory().setBoots(boots);

		ItemStack chestplate = new ItemStack(Item.LEATHER_CHESTPLATE.id);
		chestplate = Armor.setColor(chestplate, color);
		p.getInventory().setChestplate(chestplate);

		ItemStack leggings = new ItemStack(Item.LEATHER_LEGGINGS.id);
		leggings = Armor.setColor(leggings, color);
		p.getInventory().setLeggings(leggings);
		
	}
	public void pyro(Player p){
		p.getInventory().clear();
		ItemStack i = new ItemStack(Item.FLINT_AND_STEEL.id,1);
		
		ItemStack[] items = {
				i
				
		};
		p.getInventory().addItem(items);
		
		ItemStack chest = p.getInventory().getChestplate();
		chest.addEnchantment(Enchantment.PROTECTION_FIRE, 1);

		p.setHealth(10);
		p.setFoodLevel(foodlevel);
		p.sendMessage("You are now a pyro!!");
	}
	public void soldier(Player p){
		p.getInventory().clear();
		ItemStack i = new ItemStack(267,1);
		//i.addEnchantment(Enchantment.KNOCKBACK, 2);
		
		ItemStack[] items = {
				i
				
		};
		p.setHealth(19);
		p.setFoodLevel(foodlevel);
		p.getInventory().addItem(items);
		p.sendMessage("You are now a soldier!!");
	}
	public void sniper(Player p){
		p.getInventory().clear();
		ItemStack i = new ItemStack(Item.BOW.id,1);
		i.addEnchantment(Enchantment.ARROW_INFINITE, 1);
		//i.addEnchantment(Enchantment.ARROW_FIRE, 1);
		ItemStack[] items = {
				i,
				new ItemStack(Item.ARROW.id,64),
				new ItemStack(373,1, (short)16458)
				
		};
		p.getMaxHealth();
		p.setHealth(5);
		p.setFoodLevel(foodlevel);
		p.getInventory().addItem(items);
		p.sendMessage("You are now a sniper!!");
	}
	public void spleef(Player p){
		p.getInventory().clear();
		ItemStack i = new ItemStack(Item.DIAMOND_PICKAXE.id,1);
		i.addEnchantment(Enchantment.DIG_SPEED, 3);
		ItemStack[] items = {
				i
				
		};
		p.setHealth(19);
		p.setFoodLevel(foodlevel);
		p.getInventory().addItem(items);
		p.sendMessage("You can now spleef!!");
	}
	public void magic(Player p){
		p.getInventory().clear();
		ItemStack i = new ItemStack(385,32);
		//i.addEnchantment(Enchantment.ARROW_FIRE, 1);
		ItemStack[] items = {
				i,
				new ItemStack(Item.WOOD_SWORD.id,1),
				new ItemStack(373,3, (short)16389)
				
		};
		p.getMaxHealth();
		p.setHealth(5);
		p.setFoodLevel(foodlevel);
		p.getInventory().addItem(items);
		p.sendMessage("You are now a Wizard!!");
	}

	int currentloc = 0;
	
	
	public void returnBackup() {
		
		//backup_user.restore(init_user);
		for (Enumeration<BackupUser> e = backup_user.elements() ; e.hasMoreElements() ;) {
	         BackupUser u = e.nextElement();
	         Player p = init_user.getServer().getPlayer(u.getName());
	         u.restore(p);
	         p.sendMessage("You are now back to normal!!");
	         
	     }
		backup_user  =new Hashtable<String, BackupUser>();
	}
	public void leaveGame(Player pmatch) {
		
		//backup_user.restore(init_user);
		for (Enumeration<BackupUser> e = backup_user.elements() ; e.hasMoreElements() ;) {
			
	         BackupUser u = e.nextElement();
	         Player p = init_user.getServer().getPlayer(u.getName());
	         if(pmatch.getName().equals(p.getName())){
		         u.restore(p);
		         p.sendMessage("You are now back to normal!!");
		         backup_user.remove(p.getName());
		         //System.out.println(p.getName());
		         p.getServer().broadcastMessage(p.getName()+" has left the battlefield (Loser!!)");
	         }
	         
	     }
		//backup_user  =new Hashtable<String, BackupUser>();
	}
	
	

	public void cleanUp() {
		if(active_siege == true){
			active_siege = false;
			SiegeClass.root.getServer().broadcastMessage("End of the Siege");
			returnBackup();
		}
	}
	public void addSpawnLocation(Location l) {
		spawnlocations.push(l);
		
	}

	public void deathPlayer(Player p) {
		EntityDamageEvent d= p.getLastDamageCause();
		Entity e = d.getEntity();
		if(e instanceof Player){
			Player killer = (Player) e;
			init_user.getServer().broadcastMessage("Player "+p.getPlayer()+" was killed by player: ");
			init_user.getServer().broadcastMessage(""+killer.getName());
		}else if(e instanceof Monster){
			//Monster m = (Monster) e;
			init_user.getServer().broadcastMessage("Player "+p.getName()+" was a monster");
		}else{
			init_user.getServer().broadcastMessage("Player "+p.getName()+" was killed due to an unknonw cause");
		}
		
		
	}
}
