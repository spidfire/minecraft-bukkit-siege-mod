package nl.djurredeboer.siegemode;

import nl.djurredeboer.siegemode.Events.MinecraftEvents;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class Siegemode extends JavaPlugin {
	public SiegeClass siege = null;

	public void onEnable() {
		siege = new SiegeClass();
		getServer().getPluginManager().registerEvents(
				new MinecraftEvents(this), this);

	}

	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {

		if (cmd.getName().equalsIgnoreCase("dj") && args.length > 1) { // If the
																		// player
																		// typed
			// /basic then do the
			// following...

			System.out.println(args[0].toLowerCase());
			switch (args[0].toLowerCase()) {
			case "start":
				if (args.length == 4) {
					System.out.println(args[0] + " -- " + args[1] + " -- "
							+ args[2]);
					Player player = sender.getServer().getPlayer(args[1]);
					this.startSiege(player, args[2], args[3]);

				} else {

					sender.sendMessage("Wrong Parameter count");
				}

				break;
			case "leave":
				if (args.length == 2) {

					Player player = sender.getServer().getPlayer(args[1]);

					siege.leaveGame(player);
				} else {

					sender.sendMessage("Wrong Parameter count");
				}

				break;
			case "spleef":
				if (args.length == 8) {
					int x1 = Integer.parseInt(args[1]), y1 = Integer
							.parseInt(args[2]), z1 = Integer.parseInt(args[3]);
					int x2 = Integer.parseInt(args[4]), y2 = Integer
							.parseInt(args[5]), z2 = Integer.parseInt(args[6]);
					int xmin = x1 < x2 ? x1 : x2;
					int ymin = y1 < y2 ? y1 : y2;
					int zmin = z1 < z2 ? z1 : z2;
					int xto = xmin + Math.abs(x1 - x2);
					int yto = ymin + Math.abs(y1 - y2);
					int zto = zmin + Math.abs(z1 - z2);
					System.out.println("from" + xmin + "to " + xto);
					System.out.println("from" + ymin + "to " + yto);
					System.out.println("from" + zmin + "to " + zto);

					World w = getServer().getWorld(args[7]);
					if (w == null) {
						getServer().broadcastMessage(
								"Selected World does not excist");

					} else {

						for (int x = xmin; x <= xto; x++) {
							for (int y = ymin; y <= yto; y++) {
								for (int z = zmin; z <= zto; z++) {
									w.getBlockAt(x, y, z).setType(
											Material.GLASS);
								}
							}
						}
					}
				} else {

					getServer()
							.broadcastMessage(
									"Wrong Parameter count   use /dj spleef x y z x2 y2 z2 worldname");
				}

				break;
			case "backup":
				siege.returnBackup();
				if (args.length > 1) {
					String s = "";
					for (int i = 1; i < args.length; i++) {
						s = s + args[i] + " ";
					}
					getServer().broadcastMessage(s);
				}

				break;
			default:
				getServer().broadcastMessage("Unknown command..");
			}

			return true;
		}
		return false;
	}

	public void startSiege(Player p, String type, String args) {
		siege.startsiege(p, this, type, args);

	}

	public void flyspeed(String speed, Player p) {

		Float s = Float.parseFloat(speed);
		if (s > 0 && s <= 1) {
			p.sendMessage("Your new fly speed has been set");
			p.setFlySpeed(s);
		} else {
			p.sendMessage("invalid speed (use a number between the 0.0 and 1.0)");
		}

	}

	public void addSpawnLocation(Location l) {
		siege.addSpawnLocation(l);

	}

	public boolean siegeBusy() {
		return siege.active_siege;
	}

	public void storeBackup(Backupblock b) {
		siege.backup.push(b);

	}

	public void deathPlayer(Player p) {

		siege.deathPlayer(p);

	}

}
