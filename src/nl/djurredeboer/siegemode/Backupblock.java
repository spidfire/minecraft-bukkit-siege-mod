package nl.djurredeboer.siegemode;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;

public class Backupblock {
	public Location place;
	public int type = 1;
	
	public Backupblock(Block b){
		 place = b.getLocation().clone();
		 type = b.getTypeId();
	}

	public void reset(World world) {
		world.getBlockAt(place).setTypeId(type);
		
	}
}